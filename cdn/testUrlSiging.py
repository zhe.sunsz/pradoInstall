# -*- coding: utf-8 -*
import requests
import json
import sys
import time
import hashlib
import argparse

parser = argparse.ArgumentParser(description='manual to this script')

parser.add_argument('--category', type=str, default = None)

# parser.add_argument('--batch-size', type=int, default=32)
args = parser.parse_args()



def generate_url(category, ts=None):
  url = 'http://play-sp.vod.shopee.com'              # 测试域名
  path = '/s4/98934353/10321032823/czQsc2V0MjEtNzc1MywxMzc2MjAyMTQxNTE2NzA5ODg4LDE.jpg'                                     # 访问路径
  suffix = ''                                 # URL参数
  key = 'tEx8EJj7CGJpuzbm6agG6ktRV7D2cmej'                                # 鉴权密钥
  now = int(time.mktime(time.strptime(ts, "%Y%m%d%H%M%S")) if ts else time.time())                # 如果输入了时间，用输入ts，否则用当前ts
  sign_key = 'sign'                                    # url签名字段
  time_key = 't'                                      # url时间字段
  ttl_format = 10                                     # 时间进制，10或16，只有typeD支持
  if category == 'A':                                 #Type A
      ts = now
      rand_str = '123abc'
      sign = hashlib.md5('%s-%s-%s-%s-%s' % (path, ts, rand_str, 0, key)).hexdigest()
      request_url = '%s%s?%s=%s' % (url, path, sign_key, '%s-%s-%s-%s' % (ts, rand_str, 0, sign))
      print(request_url)
  elif category == 'B':                               #Type B
      ts = time.strftime('%Y%m%d%H%M', time.localtime(now))
      sign = hashlib.md5('%s%s%s' % (key, ts, path)).hexdigest()
      request_url = '%s/%s/%s%s%s' % (url, ts, sign, path, suffix)
      print(request_url)
  elif category == 'C':                               #Type C
      ts = hex(now)[2:]
      sign = hashlib.md5('%s%s%s' % (key, path, ts)).hexdigest()
      request_url = '%s/%s/%s%s%s' % (url, sign, ts, path, suffix)
      print(request_url)
  elif category == 'D':                               #Type D
      ts = now if ttl_format == 10 else hex(now)[2:]
      sign = hashlib.md5('%s%s%s' % (key, path, ts)).hexdigest()
      request_url = '%s%s?%s=%s&%s=%s' % (url, path, sign_key, sign, time_key, ts)
      print(request_url)
if __name__ == '__main__':
  if len(sys.argv) == 1:
      print('usage: python generate_url.py A 20200501000000')
  generate_url(args.category)
