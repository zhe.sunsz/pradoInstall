import requests
import json

jobid = "840e2a8f-0215-4cdc-a300-494ff1a2c10b"
url = "http://prado.infra.shopee.io/console/api/v1/app/10001/job/%s/task/submit"% jobid

bodylow = {"traceId":"low-1","priority":1,"taskContent":"lowlevel","maxRetry": 0}
bodymiddle = {"traceId":"middle-2","priority":2,"taskContent":"middlelevel","maxRetry": 0}
bodyhigh = {"traceId":"high-3","priority":3,"taskContent":"highlevel","maxRetry": 1}

headers = {
    "Content-Type":"text/plain",
    "cookie":"G_ENABLED_IDPS=google; _ga_QMDMFHB4T7=GS1.1.1613990676.1.1.1613990683.0; _ga_8SBPY6GX69=GS1.1.1615444331.21.1.1615444367.0; _ga_XWL9LM2WJ1=GS1.1.1615444331.2.1.1615444367.0; _gid=GA1.2.972947143.1615690252; G_AUTHUSER_H=0; _ga=GA1.1.1562019965.1595824123; _ga_N1FF831FCD=GS1.1.1615801234.37.1.1615801320.0; prado-console=MTYxNTgwMTQzMnxOd3dBTkVoVFZWbzFTVE0wUzBzMFRFY3pNelkzVFU4MVRWSklOVmRHVjBKQlZ6TXpVRVJKVVRSTFdVMVNSMHBSUzFZMVdGQkhTRkU9fC9t-4714R3rKHsEJLbN3lOuhISnGtR0oma1BFuHoChl; _ga_6VXJESRVZJ=GS1.1.1615802359.9.0.1615802359.0"
}

count0 = 0
count1 = 0
count2 = 0
print("LiveEnv add task--->")
for i in range(10):
    reshigh = requests.post(url=url, data=json.dumps(bodyhigh), headers=headers)
    reslow = requests.post(url=url,data=json.dumps(bodylow),headers=headers)
    resmiddle = requests.post(url=url, data=json.dumps(bodymiddle), headers=headers)
    count0 += 1 if reslow.json().get("message", None) == "ok" else 0
    count1 += 1 if resmiddle.json().get("message", None) == "ok" else 0
    count2 += 1 if reshigh.json().get("message", None) == "ok" else 0
    print(reslow.content)
    print(resmiddle.content)
    print(reshigh.content)
print(count0,count1,count2)