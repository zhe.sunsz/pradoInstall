#!/bin/sh
cd ..
if [ ! -d "./prado-scheduler" ]; then
    git clone gitlab@git.garena.com:shopee/sz-devops/middleware/prado-scheduler.git
fi
cd prado-scheduler
git reset --hard HEAD
git fetch --all
git checkout origin/release-0.1.0-rc4
git branch -vv

sed -i "" 's/10.12.78.47:2379/10.129.104.206:3346/g' ./config/config.toml
sed -i "" 's/123456/root123/g' ./config/config.toml
sed -i "" 's/10.12.78.47:3306/10.129.104.207:3306/g' ./config/config.toml
echo "server1 ansible_ssh_host=10.129.104.206\nserver2 ansible_ssh_host=10.129.104.205" > ./script/ansible/hosts
make deploy
