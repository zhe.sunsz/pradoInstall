#!/usr/bin/sh
foldername=$(date +%Y%m%d)
sudo mkdir -p /opt/prado-logs/"$foldername"/prado-agent/
sudo mkdir -p /opt/prado-logs/"$foldername"/prado-scheduler/
sudo mkdir -p /opt/prado-logs/"$foldername"/prado-console/

sudo cp -r /opt/prado-api/prado-api-0.1.0/log/prado-api /opt/prado-logs/"$foldername"/
sudo cp -r /data/log/prado_agent /opt/prado-logs/"$foldername"/prado-agent/
sudo cp -r /opt/prado-scheduler/logs /opt/prado-logs/"$foldername"/prado-scheduler/
sudo cp -r /opt/prado-console/log /opt/prado-logs/"$foldername"/prado-console/

#rm -r /opt/prado-api
#rm -r /opt/prado_agent
#rm -r /opt/prado-scheduler
#rm -r /opt/prado-console
#rm -r /opt/prado-console-0.1.0.tar.gz
#rm -r /opt/prado-scheduler-0.1.0.tar.gz



