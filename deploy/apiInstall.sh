#!/bin/sh
cd ..
if [ ! -d "./prado-api" ]; then
    git clone gitlab@git.garena.com:shopee/sz-devops/middleware/prado-api.git
fi
cd prado-api
git reset --hard HEAD
git fetch --all
git checkout origin/release-0.1.0-rc4
git branch -vv
echo "server1 ansible_ssh_host=10.129.104.206\nserver2 ansible_ssh_host=10.129.104.205" > ./ansible/hosts
make deploy
