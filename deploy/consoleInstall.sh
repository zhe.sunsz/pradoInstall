#!/bin/sh
cd ..
mv prado-console prado.console.`date +%s`
if [ ! -d "./prado-console" ]; then
    git clone gitlab@git.garena.com:shopee/sz-devops/middleware/prado-console.git
fi
cd prado-console
git reset --hard HEAD
git fetch --all
git checkout origin/release-0.1.0-rc4
git branch -vv
echo "server1 ansible_ssh_host=10.129.104.206\nserver2 ansible_ssh_host=10.129.104.205" > ./config/ansible/hosts
make deploy
