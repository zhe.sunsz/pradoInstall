#!/bin/sh
cd ..
if [ ! -d "./prado-agent" ]; then
    git clone gitlab@git.garena.com:shopee/sz-devops/middleware/prado-agent.git
fi
cd prado-agent
git reset --hard HEAD
git fetch --all
git checkout origin/release-0.2.0-rc1
git branch -vv
sed -i "" 's/10.12.78.47:2379/10.129.104.206:3346/g' config.toml
sed -i "" 's/10.12.78.47:80/10.129.104.206:15000/g' config.toml
#sed -i "" 's//data/log/prado_agent/agent_logs///opt/prado_agent/logs/g' config.toml
echo "server1 ansible_ssh_host=10.129.104.101\nserver2 ansible_ssh_host=10.129.104.205\nserver3 ansible_ssh_host=10.129.104.206\nserver4 ansible_ssh_host=10.129.104.207\n" > ./ansible/hosts
make && cd ansible &&ansible-playbook -i hosts display.yml -e role=prado_agent ;cd ..
cd ..