import requests
import time

busy = []
def getBusy():
    url = "http://prado.devops.i.test.sz.shopee.io/console/api/v1/app/10030/job/3b060d07-891a-4ae7-8778-46860e32d34c/instance/list?sort=%5B%7B%7D%5D&searchBy=&status=normal&executeStatus=1&page=1&limit=10"
    header = {
        "cookie": "G_ENABLED_IDPS=google; _ga_6VXJESRVZJ=GS1.1.1618483422.12.1.1618483584.0; G_AUTHUSER_H=0; _gid=GA1.2.392641871.1624860194; _ga_QMDMFHB4T7=GS1.1.1625045559.3.1.1625046452.0; prado-console=MTYyNTcyODc2OHxOd3dBTkVVMlRrOHpOMDVWVTBkVFZrNVhUMHcyVWtGVVVWTkNWMFkwUlRaRVNrbEpWVlJEVVZwT1VsWkNSMWRQV0ZNeU5GTkJOa0U9fGv3z_hUV-mEblXUcNpVW4yrynXWONMPocvSCR-KM9Wp; _ga_5H3PPWG2TZ=GS1.1.1625729361.1.1.1625729415.0; _ga_4YS12BJ90C=GS1.1.1625729375.1.0.1625729418.0; _ga_XWL9LM2WJ1=GS1.1.1625738719.21.0.1625740036.0; _ga_8SBPY6GX69=GS1.1.1625740036.40.0.1625740036.0; _ga=GA1.2.1562019965.1595824123; _ga_N1FF831FCD=GS1.1.1626229981.70.0.1626229981.0",

    }
    resp = requests.get(url=url, headers=header)
    resp = resp.json()
    # print resp
    if resp['code'] == 0:
        busy.append(resp['data']['allTotals']['normal'])
    else:
        print("not ok ")
        return False

def Max(busylist):
    max = 0
    if len(busylist)>0:
        max = busylist[0]
        for i in range(len(busylist)):
            if max < busylist[i]:
                max = busylist[i]
    return max

def monitor():
    while True:
        getBusy()
        print Max(busy)
        time.sleep(4)

monitor()

