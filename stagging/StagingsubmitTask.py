import requests
import json

jobid = "76f5c3a1-3163-4dae-8b5b-69c5e309aef4"
url = "http://prado.infra.staging.shopee.io/console/api/v1/app/10003/job/%s/task/submit"% jobid

bodylow = {"traceId":"low-1","priority":1,"taskContent":"lowlevel","maxRetry": 0}
bodymiddle = {"traceId":"middle-2","priority":2,"taskContent":"middlelevel","maxRetry": 0}
bodyhigh = {"traceId":"high-3","priority":3,"taskContent":"highlevel","maxRetry": 1}

headers = {
    "Content-Type":"application/json; charset=utf-8",
    "cookie":"G_ENABLED_IDPS=google; _ga_XWL9LM2WJ1=GS1.1.1612510259.1.1.1612510509.0; _ga_8SBPY6GX69=GS1.1.1612510259.20.1.1612510509.0; _ga_QMDMFHB4T7=GS1.1.1613990676.1.1.1613990683.0; _ga_6VXJESRVZJ=GS1.1.1614570493.5.0.1614571007.0; _gid=GA1.2.1782808222.1615170720; _ga_N1FF831FCD=GS1.1.1615285245.26.0.1615285245.0; _ga=GA1.2.1562019965.1595824123; G_AUTHUSER_H=0; prado-console=MTYxNTM2NDQwNXxOd3dBTkU1UlQwSlFTRkJCUkVoUVUwSkRRelkzU1VWYVNGZFdVMWN5UjBOUlQwWkNUbEJGUTBKSVJVSktSVFphTjBjMlZ6SllVa0U9fBRw8MLhbP74WbPzZ88kYIHVmyAWnC9WcPHhFzGu-qk2"
}

count0 = 0
count1 = 0
count2 = 0
print("StagingEnv add task--->")
for i in range(1000):
    reshigh = requests.post(url=url, data=json.dumps(bodyhigh), headers=headers)
    reslow = requests.post(url=url,data=json.dumps(bodylow),headers=headers)
    resmiddle = requests.post(url=url, data=json.dumps(bodymiddle), headers=headers)
    count0 += 1 if reslow.json().get("message", None) == "ok" else 0
    count1 += 1 if resmiddle.json().get("message", None) == "ok" else 0
    count2 += 1 if reshigh.json().get("message", None) == "ok" else 0
    print(reslow.content)
    print(resmiddle.content)
    print(reshigh.content)
print(count0,count1,count2)