import requests
import json

appid = "10001"
appsecret = "VQkeSdgrGqudtVRPmdyGEaAbBUSVSKwX"
jobGroupId = "25b60c83-61d3-4f6e-9578-f9fb8bc6d31e"

# get access_token
tokenUrl = "http://prado.infra.staging.shopee.io/oauth/api/v1/token"
tokenHeaders = {
	"Content-Type": "application/json",
	"appid": appid,
	"appsecret": appsecret
}
tokenRes = requests.get(url=tokenUrl,headers=tokenHeaders)
accessToken = tokenRes.json().get("data").get("access_token")
print("access_token: ",accessToken)

url = "http://prado.infra.staging.shopee.io/openapi/v1/task/submitByGroup"
lowbody = {
     "traceId": "low-python脚本",
	 "jobGroupId": jobGroupId,
	 "taskContent": "maxretry-低优先级task",
	 "priority": 1,
	 "createBy": "zhe.sunsz@shopee.com",
	 "maxRetry": 1
}
midbody = {
     "traceId": "middle-python脚本",
	 "jobGroupId": jobGroupId,
	 "taskContent": "maxretry-中优先级task",
	 "priority": 2,
	 "createBy": "zhe.sunsz@shopee.com",
	 "maxRetry": 2
}
highbody = {
     "traceId": "high-python脚本",
	 "jobGroupId": jobGroupId,
	 "taskContent": "maxretry-高优先级task",
	 "priority": 3,
	 "createBy": "zhe.sunsz@shopee.com",
	 "maxRetry": 3
}

headers = {
    "Content-Type": "application/json; charset=utf-8",
	"Authorization":accessToken
}

count0 = 0
count1 = 0
count2 = 0
print("StagingEnv submit task by jobgroup--->")
for i in range(1):
	lowres = requests.post(url=url,data=json.dumps(lowbody),headers=headers)
	midres = requests.post(url=url, data=json.dumps(midbody), headers=headers)
	highres = requests.post(url=url, data=json.dumps(highbody), headers=headers)
	count0 += 1 if lowres.json().get("message", None) == "ok" else 0
	count1 += 1 if midres.json().get("message", None) == "ok" else 0
	count2 += 1 if highres.json().get("message", None) == "ok" else 0
	print(lowres.content)
	print(midres.content)
	print(highres.content)
print(count0,count1,count2)