import requests
import json

appid = "10028"
appsecret = "jXmbcEdbRRhxVMtqNvmMCxrfHMKwahYM"
# appid = "10010"
# appsecret = "mTfgVdnLvRqXbikjiQyArzSohGCbieEm"
# jobid = "b7a9d42e-e2b4-47ae-98fc-b38376585210" # cpu-type-job-noautoScale
# jobid = "cdf7db3a-09f4-4e91-b83f-93ae3b7707c1" # panicInstance
# jobid = "2a735186-c3dc-4987-9a52-99ab0a3291a2" # long-cpu-NScale
# jobid = "f06e6f65-1b10-4bf7-87c1-9e4409af5cb4" # stress v2
# jobid = "96d46ee5-abae-4c5b-b073-4ec160d65d9a" # stress v4
# jobid = "533f1752-a70e-47e2-90a3-0779936ec6f1" # stress v5
jobid = "5197f45e-9f42-48d1-9d36-b5589b394d57"


# get access_token
tokenUrl = "http://prado.devops.i.test.sz.shopee.io/oauth/api/v1/token"
tokenHeaders = {
	"Content-Type": "application/json",
	"appid": appid,
	"appsecret": appsecret
}
tokenRes = requests.get(url=tokenUrl,headers=tokenHeaders)
accessToken = tokenRes.json().get("data").get("access_token")
print(accessToken)

# submit task
url = "http://prado.devops.i.test.sz.shopee.io/openapi/v2/task/submit"
bodylow = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "low task:www.baidu.com",
  "priority": 1,
  "createBy": "python",
  "callbackUrl": "www.test.com"
}
bodymiddle = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "medium task:www.baidu.com",
  "priority": 2,
  "createBy": "python",
  "callbackUrl": "www.middle.com"
}
bodyhigh = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "high task:www.shopee.com",
  "priority": 3,
  "createBy": "python",
  "callbackUrl": "www.test.com"
}

headers = {
    "Content-Type":"application/json; charset=utf-8",
    "Authorization": accessToken
}

count0 = 0
count1 = 0
count2 = 0
print("add task--->")
for i in range(0):
    reshigh = requests.post(url=url, data=json.dumps(bodyhigh), headers=headers)
    reslow = requests.post(url=url,data=json.dumps(bodylow),headers=headers)
    resmiddle = requests.post(url=url, data=json.dumps(bodymiddle), headers=headers)
    count0 += 1 if reslow.json().get("message", None) == "ok" else 0
    count1 += 1 if resmiddle.json().get("message", None) == "ok" else 0
    count2 += 1 if reshigh.json().get("message", None) == "ok" else 0
    print(reslow.content)
    print(resmiddle.content)
    print(reshigh.content)
print(count0,count1,count2)


