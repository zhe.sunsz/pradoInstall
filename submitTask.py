import requests
import json

# appid = "10028"
# appsecret = "jXmbcEdbRRhxVMtqNvmMCxrfHMKwahYM"
appid = "10003"
appsecret = "hWKRHMmRhBAjbaNtdKTWGfqIaSNCayPD"
# appid = "10010"
# appsecret = "mTfgVdnLvRqXbikjiQyArzSohGCbieEm"

# jobid = "94c37448-cc00-4e3c-8049-89eb5518f16c" # cpu_no_autoScale_job
# jobid = "2563258a-a860-4cfd-99b8-1af0a678ff78" # cpu_autoScale_job
# jobid = "60be4dd2-5054-47f4-988d-d6d64613cbbb" # cpuAutoScale
jobid = "c6924bfb-09b4-4550-9161-6797e505de40"

# get access_token
tokenUrl = "http://prado.devops.i.test.sz.shopee.io/oauth/api/v1/token"
tokenHeaders = {
	"Content-Type": "application/json",
	"appid": appid,
	"appsecret": appsecret
}
tokenRes = requests.get(url=tokenUrl,headers=tokenHeaders)
accessToken = tokenRes.json().get("data").get("access_token")
print(accessToken)
# submit task

url = "http://prado.devops.i.test.sz.shopee.io/openapi/v1/task/submit"
bodylow = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "low task:www.baidu.com",
  "priority": 1,
  "createBy": "python",
  "maxRetry": 1
}
bodymiddle = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "middle task:www.shopee.com",
  "priority": 2,
  "createBy": "python",
  "maxRetry": 2
}
bodyhigh = {
  "traceId": "",
  "jobId": jobid,
  "taskContent": "high task",
  "priority": 3,
  "createBy": "python",
  "maxRetry": 3
}

headers = {
    "Content-Type":"application/json; charset=utf-8",
    "Authorization": accessToken
}

count0 = 0
count1 = 0
count2 = 0
print("add task--->")
for i in range(0):
    reshigh = requests.post(url=url, data=json.dumps(bodyhigh), headers=headers)
    reslow = requests.post(url=url,data=json.dumps(bodylow),headers=headers)
    resmiddle = requests.post(url=url, data=json.dumps(bodymiddle), headers=headers)
    count0 += 1 if reslow.json().get("message", None) == "ok" else 0
    count1 += 1 if resmiddle.json().get("message", None) == "ok" else 0
    count2 += 1 if reshigh.json().get("message", None) == "ok" else 0
    print(reshigh.content)
    print(resmiddle.content)
    print(reshigh.content)
print(count0,count1,count2)