import requests
import json

appid = "10028"
appsecret = "jXmbcEdbRRhxVMtqNvmMCxrfHMKwahYM"
jobGroupId = "f70af1d5-2e71-4069-9d34-15347abf6608"

# get access_token
tokenUrl = "http://prado.devops.i.test.sz.shopee.io/oauth/api/v1/token"
tokenHeaders = {
	"Content-Type": "application/json",
	"appid": appid,
	"appsecret": appsecret
}
tokenRes = requests.get(url=tokenUrl,headers=tokenHeaders)
accessToken = tokenRes.json().get("data").get("access_token")
print(accessToken)

# submit task by job group
url = "http://prado.devops.i.test.sz.shopee.io/openapi/v2/task/submitByGroup"
lowbody = {
  "traceId": "",
  "jobGroupId": jobGroupId,
  "taskContent": "low-task",
  "priority": 1,
  "createBy": "pyhton-group",
  "callbackUrl": ""
}
midbody = {
  "traceId": "",
  "jobGroupId": jobGroupId,
  "taskContent": "medium-task",
  "priority": 2,
  "createBy": "pyhton-group",
  "callbackUrl": ""
}
highbody = {
  "traceId": "",
  "jobGroupId": jobGroupId,
  "taskContent": "medium-task",
  "priority": 2,
  "createBy": "pyhton-group",
  "callbackUrl": ""
}

headers = {
    "Content-Type": "application/json; charset=utf-8",
	"Authorization": accessToken

}

count0 = 0
count1 = 0
count2 = 0
print("submit task by jobgroup--->")
for i in range(0):
	lowres = requests.post(url=url,data=json.dumps(lowbody),headers=headers)
	midres = requests.post(url=url, data=json.dumps(midbody), headers=headers)
	highres = requests.post(url=url, data=json.dumps(highbody), headers=headers)
	count0 += 1 if lowres.json().get("message", None) == "ok" else 0
	count1 += 1 if midres.json().get("message", None) == "ok" else 0
	count2 += 1 if highres.json().get("message", None) == "ok" else 0
	print(lowres.content)
	print(midres.content)
	print(highres.content)
print(count0,count1,count2)